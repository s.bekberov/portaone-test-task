package com.example;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String filePath = "file/10m.txt";

        try {
            processFile(filePath);
        } catch (IOException e) {
            System.err.println("An error occurred while processing the file: " + e.getMessage());
        }
    }

    private static void processFile(String filePath) throws IOException {
        long count = 0;
        long sum = 0;
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        PriorityQueue<Integer> lower = new PriorityQueue<>(Collections.reverseOrder());
        PriorityQueue<Integer> higher = new PriorityQueue<>();
        int longestIncreasingLength = 0, currentIncreasingLength = 1;
        int longestDecreasingLength = 0, currentDecreasingLength = 1;
        int prevNumber = Integer.MIN_VALUE;

        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                int number = Integer.parseInt(line);
                sum += number;
                count++;
                max = Math.max(max, number);
                min = Math.min(min, number);

                if (lower.isEmpty() || number <= lower.peek()) {
                    lower.add(number);
                } else {
                    higher.add(number);
                }
                balanceHeaps(lower, higher);

                if (prevNumber != Integer.MIN_VALUE) {
                    if (number > prevNumber) {
                        currentIncreasingLength++;
                        longestIncreasingLength = Math.max(longestIncreasingLength, currentIncreasingLength);
                        currentDecreasingLength = 1;
                    } else if (number < prevNumber) {
                        currentDecreasingLength++;
                        longestDecreasingLength = Math.max(longestDecreasingLength, currentDecreasingLength);
                        currentIncreasingLength = 1;
                    } else {
                        currentIncreasingLength = 1;
                        currentDecreasingLength = 1;
                    }
                }
                prevNumber = number;
            }
        }

        double median = getMedian(lower, higher, count);
        double average = (double) sum / count;

        System.out.println("Maximum: " + max);
        System.out.println("Minimum: " + min);
        System.out.println("Median: " + median);
        System.out.println("Average: " + average);
        System.out.println("Longest Increasing Sequence Length: " + longestIncreasingLength);
        System.out.println("Longest Decreasing Sequence Length: " + longestDecreasingLength);
    }

    private static void balanceHeaps(PriorityQueue<Integer> lower, PriorityQueue<Integer> higher) {
        PriorityQueue<Integer> biggerHeap = (lower.size() > higher.size()) ? lower : higher;
        PriorityQueue<Integer> smallerHeap = (lower.size() > higher.size()) ? higher : lower;

        if (biggerHeap.size() - smallerHeap.size() >= 2) {
            smallerHeap.add(biggerHeap.poll());
        }
    }

    private static double getMedian(PriorityQueue<Integer> lower, PriorityQueue<Integer> higher, long count) {
        if (lower.size() == higher.size()) {
            return ((double) lower.peek() + (double) higher.peek()) / 2;
        } else {
            return (lower.size() > higher.size()) ? lower.peek() : higher.peek();
        }
    }
}